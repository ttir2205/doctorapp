FROM openjdk:8
EXPOSE 8080
ADD target/doctor-app.jar doctor-app.jar
ENTRYPOINT ["java","-jar","/doctor-app.jar"]