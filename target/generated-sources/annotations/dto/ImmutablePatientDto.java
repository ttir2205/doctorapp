package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import org.immutables.value.Generated;

/**
 * Immutable implementation of {@link PatientDto}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutablePatientDto.builder()}.
 */
@Generated(from = "PatientDto", generator = "Immutables")
@SuppressWarnings({"all"})
@ParametersAreNonnullByDefault
@javax.annotation.Generated("org.immutables.processor.ProxyProcessor")
@Immutable
public final class ImmutablePatientDto implements PatientDto {
  private final String firstName;
  private final String lastName;
  private final String gender;
  private final Date birthDate;
  private final @Nullable UUID id;
  private final @Nullable ImmutableList<MedicationDto> medications;

  private ImmutablePatientDto(
      String firstName,
      String lastName,
      String gender,
      Date birthDate,
      @Nullable UUID id,
      @Nullable ImmutableList<MedicationDto> medications) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.birthDate = birthDate;
    this.id = id;
    this.medications = medications;
  }

  /**
   * @return The value of the {@code firstName} attribute
   */
  @JsonProperty("firstName")
  @Override
  public String getFirstName() {
    return firstName;
  }

  /**
   * @return The value of the {@code lastName} attribute
   */
  @JsonProperty("lastName")
  @Override
  public String getLastName() {
    return lastName;
  }

  /**
   * @return The value of the {@code gender} attribute
   */
  @JsonProperty("gender")
  @Override
  public String getGender() {
    return gender;
  }

  /**
   * @return The value of the {@code birthDate} attribute
   */
  @JsonProperty("birthDate")
  @Override
  public Date getBirthDate() {
    return birthDate;
  }

  /**
   * @return The value of the {@code id} attribute
   */
  @JsonProperty("id")
  @Override
  public @Nullable UUID getId() {
    return id;
  }

  /**
   * @return The value of the {@code medications} attribute
   */
  @JsonProperty("medications")
  @Override
  public @Nullable ImmutableList<MedicationDto> getMedications() {
    return medications;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link PatientDto#getFirstName() firstName} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for firstName
   * @return A modified copy of the {@code this} object
   */
  public final ImmutablePatientDto withFirstName(String value) {
    String newValue = Objects.requireNonNull(value, "firstName");
    if (this.firstName.equals(newValue)) return this;
    return new ImmutablePatientDto(newValue, this.lastName, this.gender, this.birthDate, this.id, this.medications);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link PatientDto#getLastName() lastName} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for lastName
   * @return A modified copy of the {@code this} object
   */
  public final ImmutablePatientDto withLastName(String value) {
    String newValue = Objects.requireNonNull(value, "lastName");
    if (this.lastName.equals(newValue)) return this;
    return new ImmutablePatientDto(this.firstName, newValue, this.gender, this.birthDate, this.id, this.medications);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link PatientDto#getGender() gender} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for gender
   * @return A modified copy of the {@code this} object
   */
  public final ImmutablePatientDto withGender(String value) {
    String newValue = Objects.requireNonNull(value, "gender");
    if (this.gender.equals(newValue)) return this;
    return new ImmutablePatientDto(this.firstName, this.lastName, newValue, this.birthDate, this.id, this.medications);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link PatientDto#getBirthDate() birthDate} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for birthDate
   * @return A modified copy of the {@code this} object
   */
  public final ImmutablePatientDto withBirthDate(Date value) {
    if (this.birthDate == value) return this;
    Date newValue = Objects.requireNonNull(value, "birthDate");
    return new ImmutablePatientDto(this.firstName, this.lastName, this.gender, newValue, this.id, this.medications);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link PatientDto#getId() id} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for id (can be {@code null})
   * @return A modified copy of the {@code this} object
   */
  public final ImmutablePatientDto withId(@Nullable UUID value) {
    if (this.id == value) return this;
    return new ImmutablePatientDto(this.firstName, this.lastName, this.gender, this.birthDate, value, this.medications);
  }

  /**
   * Copy the current immutable object with elements that replace the content of {@link PatientDto#getMedications() medications}.
   * @param elements The elements to set
   * @return A modified copy of {@code this} object
   */
  public final ImmutablePatientDto withMedications(@Nullable MedicationDto... elements) {
    if (elements == null) {
      return new ImmutablePatientDto(this.firstName, this.lastName, this.gender, this.birthDate, this.id, null);
    }
    @Nullable ImmutableList<MedicationDto> newValue = elements == null ? null : ImmutableList.copyOf(elements);
    return new ImmutablePatientDto(this.firstName, this.lastName, this.gender, this.birthDate, this.id, newValue);
  }

  /**
   * Copy the current immutable object with elements that replace the content of {@link PatientDto#getMedications() medications}.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param elements An iterable of medications elements to set
   * @return A modified copy of {@code this} object
   */
  public final ImmutablePatientDto withMedications(@Nullable Iterable<? extends MedicationDto> elements) {
    if (this.medications == elements) return this;
    @Nullable ImmutableList<MedicationDto> newValue = elements == null ? null : ImmutableList.copyOf(elements);
    return new ImmutablePatientDto(this.firstName, this.lastName, this.gender, this.birthDate, this.id, newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutablePatientDto} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(@Nullable Object another) {
    if (this == another) return true;
    return another instanceof ImmutablePatientDto
        && equalTo((ImmutablePatientDto) another);
  }

  private boolean equalTo(ImmutablePatientDto another) {
    return firstName.equals(another.firstName)
        && lastName.equals(another.lastName)
        && gender.equals(another.gender)
        && birthDate.equals(another.birthDate)
        && Objects.equals(id, another.id)
        && Objects.equals(medications, another.medications);
  }

  /**
   * Computes a hash code from attributes: {@code firstName}, {@code lastName}, {@code gender}, {@code birthDate}, {@code id}, {@code medications}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + firstName.hashCode();
    h += (h << 5) + lastName.hashCode();
    h += (h << 5) + gender.hashCode();
    h += (h << 5) + birthDate.hashCode();
    h += (h << 5) + Objects.hashCode(id);
    h += (h << 5) + Objects.hashCode(medications);
    return h;
  }

  /**
   * Prints the immutable value {@code PatientDto} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper("PatientDto")
        .omitNullValues()
        .add("firstName", firstName)
        .add("lastName", lastName)
        .add("gender", gender)
        .add("birthDate", birthDate)
        .add("id", id)
        .add("medications", medications)
        .toString();
  }

  /**
   * Creates an immutable copy of a {@link PatientDto} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable PatientDto instance
   */
  public static ImmutablePatientDto copyOf(PatientDto instance) {
    if (instance instanceof ImmutablePatientDto) {
      return (ImmutablePatientDto) instance;
    }
    return ImmutablePatientDto.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link ImmutablePatientDto ImmutablePatientDto}.
   * <pre>
   * ImmutablePatientDto.builder()
   *    .firstName(String) // required {@link PatientDto#getFirstName() firstName}
   *    .lastName(String) // required {@link PatientDto#getLastName() lastName}
   *    .gender(String) // required {@link PatientDto#getGender() gender}
   *    .birthDate(Date) // required {@link PatientDto#getBirthDate() birthDate}
   *    .id(UUID | null) // nullable {@link PatientDto#getId() id}
   *    .medications(List&amp;lt;dto.MedicationDto&amp;gt; | null) // nullable {@link PatientDto#getMedications() medications}
   *    .build();
   * </pre>
   * @return A new ImmutablePatientDto builder
   */
  public static ImmutablePatientDto.Builder builder() {
    return new ImmutablePatientDto.Builder();
  }

  /**
   * Builds instances of type {@link ImmutablePatientDto ImmutablePatientDto}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @Generated(from = "PatientDto", generator = "Immutables")
  @NotThreadSafe
  public static final class Builder {
    private static final long INIT_BIT_FIRST_NAME = 0x1L;
    private static final long INIT_BIT_LAST_NAME = 0x2L;
    private static final long INIT_BIT_GENDER = 0x4L;
    private static final long INIT_BIT_BIRTH_DATE = 0x8L;
    private long initBits = 0xfL;

    private @Nullable String firstName;
    private @Nullable String lastName;
    private @Nullable String gender;
    private @Nullable Date birthDate;
    private @Nullable UUID id;
    private ImmutableList.Builder<MedicationDto> medications = null;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code PatientDto} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * Collection elements and entries will be added, not replaced.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(PatientDto instance) {
      Objects.requireNonNull(instance, "instance");
      firstName(instance.getFirstName());
      lastName(instance.getLastName());
      gender(instance.getGender());
      birthDate(instance.getBirthDate());
      @Nullable UUID idValue = instance.getId();
      if (idValue != null) {
        id(idValue);
      }
      @Nullable List<MedicationDto> medicationsValue = instance.getMedications();
      if (medicationsValue != null) {
        addAllMedications(medicationsValue);
      }
      return this;
    }

    /**
     * Initializes the value for the {@link PatientDto#getFirstName() firstName} attribute.
     * @param firstName The value for firstName 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("firstName")
    public final Builder firstName(String firstName) {
      this.firstName = Objects.requireNonNull(firstName, "firstName");
      initBits &= ~INIT_BIT_FIRST_NAME;
      return this;
    }

    /**
     * Initializes the value for the {@link PatientDto#getLastName() lastName} attribute.
     * @param lastName The value for lastName 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("lastName")
    public final Builder lastName(String lastName) {
      this.lastName = Objects.requireNonNull(lastName, "lastName");
      initBits &= ~INIT_BIT_LAST_NAME;
      return this;
    }

    /**
     * Initializes the value for the {@link PatientDto#getGender() gender} attribute.
     * @param gender The value for gender 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("gender")
    public final Builder gender(String gender) {
      this.gender = Objects.requireNonNull(gender, "gender");
      initBits &= ~INIT_BIT_GENDER;
      return this;
    }

    /**
     * Initializes the value for the {@link PatientDto#getBirthDate() birthDate} attribute.
     * @param birthDate The value for birthDate 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("birthDate")
    public final Builder birthDate(Date birthDate) {
      this.birthDate = Objects.requireNonNull(birthDate, "birthDate");
      initBits &= ~INIT_BIT_BIRTH_DATE;
      return this;
    }

    /**
     * Initializes the value for the {@link PatientDto#getId() id} attribute.
     * @param id The value for id (can be {@code null})
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("id")
    public final Builder id(@Nullable UUID id) {
      this.id = id;
      return this;
    }

    /**
     * Adds one element to {@link PatientDto#getMedications() medications} list.
     * @param element A medications element
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addMedications(MedicationDto element) {
      if (this.medications == null) {
        this.medications = ImmutableList.builder();
      }
      this.medications.add(element);
      return this;
    }

    /**
     * Adds elements to {@link PatientDto#getMedications() medications} list.
     * @param elements An array of medications elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addMedications(MedicationDto... elements) {
      if (this.medications == null) {
        this.medications = ImmutableList.builder();
      }
      this.medications.add(elements);
      return this;
    }


    /**
     * Sets or replaces all elements for {@link PatientDto#getMedications() medications} list.
     * @param elements An iterable of medications elements
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("medications")
    public final Builder medications(@Nullable Iterable<? extends MedicationDto> elements) {
      if (elements == null) {
        this.medications = null;
        return this;
      }
      this.medications = ImmutableList.builder();
      return addAllMedications(elements);
    }

    /**
     * Adds elements to {@link PatientDto#getMedications() medications} list.
     * @param elements An iterable of medications elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addAllMedications(Iterable<? extends MedicationDto> elements) {
      Objects.requireNonNull(elements, "medications element");
      if (this.medications == null) {
        this.medications = ImmutableList.builder();
      }
      this.medications.addAll(elements);
      return this;
    }

    /**
     * Builds a new {@link ImmutablePatientDto ImmutablePatientDto}.
     * @return An immutable instance of PatientDto
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutablePatientDto build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutablePatientDto(firstName, lastName, gender, birthDate, id, medications == null ? null : medications.build());
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = new ArrayList<>();
      if ((initBits & INIT_BIT_FIRST_NAME) != 0) attributes.add("firstName");
      if ((initBits & INIT_BIT_LAST_NAME) != 0) attributes.add("lastName");
      if ((initBits & INIT_BIT_GENDER) != 0) attributes.add("gender");
      if ((initBits & INIT_BIT_BIRTH_DATE) != 0) attributes.add("birthDate");
      return "Cannot build PatientDto, some of required attributes are not set " + attributes;
    }
  }
}
