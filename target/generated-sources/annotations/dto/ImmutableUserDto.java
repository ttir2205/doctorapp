package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import org.immutables.value.Generated;

/**
 * Immutable implementation of {@link UserDto}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableUserDto.builder()}.
 */
@Generated(from = "UserDto", generator = "Immutables")
@SuppressWarnings({"all"})
@ParametersAreNonnullByDefault
@javax.annotation.Generated("org.immutables.processor.ProxyProcessor")
@Immutable
public final class ImmutableUserDto implements UserDto {
  private final String username;
  private final @Nullable String password;

  private ImmutableUserDto(String username, @Nullable String password) {
    this.username = username;
    this.password = password;
  }

  /**
   * @return The value of the {@code username} attribute
   */
  @JsonProperty("username")
  @Override
  public String getUsername() {
    return username;
  }

  /**
   * @return The value of the {@code password} attribute
   */
  @JsonProperty("password")
  @Override
  public @Nullable String getPassword() {
    return password;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link UserDto#getUsername() username} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for username
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableUserDto withUsername(String value) {
    String newValue = Objects.requireNonNull(value, "username");
    if (this.username.equals(newValue)) return this;
    return new ImmutableUserDto(newValue, this.password);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link UserDto#getPassword() password} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for password (can be {@code null})
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableUserDto withPassword(@Nullable String value) {
    if (Objects.equals(this.password, value)) return this;
    return new ImmutableUserDto(this.username, value);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableUserDto} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(@Nullable Object another) {
    if (this == another) return true;
    return another instanceof ImmutableUserDto
        && equalTo((ImmutableUserDto) another);
  }

  private boolean equalTo(ImmutableUserDto another) {
    return username.equals(another.username)
        && Objects.equals(password, another.password);
  }

  /**
   * Computes a hash code from attributes: {@code username}, {@code password}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + username.hashCode();
    h += (h << 5) + Objects.hashCode(password);
    return h;
  }

  /**
   * Prints the immutable value {@code UserDto} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper("UserDto")
        .omitNullValues()
        .add("username", username)
        .add("password", password)
        .toString();
  }

  /**
   * Creates an immutable copy of a {@link UserDto} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable UserDto instance
   */
  public static ImmutableUserDto copyOf(UserDto instance) {
    if (instance instanceof ImmutableUserDto) {
      return (ImmutableUserDto) instance;
    }
    return ImmutableUserDto.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link ImmutableUserDto ImmutableUserDto}.
   * <pre>
   * ImmutableUserDto.builder()
   *    .username(String) // required {@link UserDto#getUsername() username}
   *    .password(String | null) // nullable {@link UserDto#getPassword() password}
   *    .build();
   * </pre>
   * @return A new ImmutableUserDto builder
   */
  public static ImmutableUserDto.Builder builder() {
    return new ImmutableUserDto.Builder();
  }

  /**
   * Builds instances of type {@link ImmutableUserDto ImmutableUserDto}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @Generated(from = "UserDto", generator = "Immutables")
  @NotThreadSafe
  public static final class Builder {
    private static final long INIT_BIT_USERNAME = 0x1L;
    private long initBits = 0x1L;

    private @Nullable String username;
    private @Nullable String password;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code UserDto} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(UserDto instance) {
      Objects.requireNonNull(instance, "instance");
      username(instance.getUsername());
      @Nullable String passwordValue = instance.getPassword();
      if (passwordValue != null) {
        password(passwordValue);
      }
      return this;
    }

    /**
     * Initializes the value for the {@link UserDto#getUsername() username} attribute.
     * @param username The value for username 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("username")
    public final Builder username(String username) {
      this.username = Objects.requireNonNull(username, "username");
      initBits &= ~INIT_BIT_USERNAME;
      return this;
    }

    /**
     * Initializes the value for the {@link UserDto#getPassword() password} attribute.
     * @param password The value for password (can be {@code null})
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("password")
    public final Builder password(@Nullable String password) {
      this.password = password;
      return this;
    }

    /**
     * Builds a new {@link ImmutableUserDto ImmutableUserDto}.
     * @return An immutable instance of UserDto
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableUserDto build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutableUserDto(username, password);
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = new ArrayList<>();
      if ((initBits & INIT_BIT_USERNAME) != 0) attributes.add("username");
      return "Cannot build UserDto, some of required attributes are not set " + attributes;
    }
  }
}
