package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import org.immutables.value.Generated;

/**
 * Immutable implementation of {@link MedicationDto}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableMedicationDto.builder()}.
 */
@Generated(from = "MedicationDto", generator = "Immutables")
@SuppressWarnings({"all"})
@ParametersAreNonnullByDefault
@javax.annotation.Generated("org.immutables.processor.ProxyProcessor")
@Immutable
public final class ImmutableMedicationDto implements MedicationDto {
  private final @Nullable UUID id;
  private final String description;
  private final Float dosage;
  private final String unit;
  private final LocalTime timeToTake;

  private ImmutableMedicationDto(
      @Nullable UUID id,
      String description,
      Float dosage,
      String unit,
      LocalTime timeToTake) {
    this.id = id;
    this.description = description;
    this.dosage = dosage;
    this.unit = unit;
    this.timeToTake = timeToTake;
  }

  /**
   * @return The value of the {@code id} attribute
   */
  @JsonProperty("id")
  @Override
  public @Nullable UUID getId() {
    return id;
  }

  /**
   * @return The value of the {@code description} attribute
   */
  @JsonProperty("description")
  @Override
  public String getDescription() {
    return description;
  }

  /**
   * @return The value of the {@code dosage} attribute
   */
  @JsonProperty("dosage")
  @Override
  public Float getDosage() {
    return dosage;
  }

  /**
   * @return The value of the {@code unit} attribute
   */
  @JsonProperty("unit")
  @Override
  public String getUnit() {
    return unit;
  }

  /**
   * @return The value of the {@code timeToTake} attribute
   */
  @JsonProperty("timeToTake")
  @Override
  public LocalTime getTimeToTake() {
    return timeToTake;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MedicationDto#getId() id} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for id (can be {@code null})
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMedicationDto withId(@Nullable UUID value) {
    if (this.id == value) return this;
    return new ImmutableMedicationDto(value, this.description, this.dosage, this.unit, this.timeToTake);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MedicationDto#getDescription() description} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for description
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMedicationDto withDescription(String value) {
    String newValue = Objects.requireNonNull(value, "description");
    if (this.description.equals(newValue)) return this;
    return new ImmutableMedicationDto(this.id, newValue, this.dosage, this.unit, this.timeToTake);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MedicationDto#getDosage() dosage} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for dosage
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMedicationDto withDosage(Float value) {
    Float newValue = Objects.requireNonNull(value, "dosage");
    if (this.dosage.equals(newValue)) return this;
    return new ImmutableMedicationDto(this.id, this.description, newValue, this.unit, this.timeToTake);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MedicationDto#getUnit() unit} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for unit
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMedicationDto withUnit(String value) {
    String newValue = Objects.requireNonNull(value, "unit");
    if (this.unit.equals(newValue)) return this;
    return new ImmutableMedicationDto(this.id, this.description, this.dosage, newValue, this.timeToTake);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MedicationDto#getTimeToTake() timeToTake} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for timeToTake
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMedicationDto withTimeToTake(LocalTime value) {
    if (this.timeToTake == value) return this;
    LocalTime newValue = Objects.requireNonNull(value, "timeToTake");
    return new ImmutableMedicationDto(this.id, this.description, this.dosage, this.unit, newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableMedicationDto} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(@Nullable Object another) {
    if (this == another) return true;
    return another instanceof ImmutableMedicationDto
        && equalTo((ImmutableMedicationDto) another);
  }

  private boolean equalTo(ImmutableMedicationDto another) {
    return Objects.equals(id, another.id)
        && description.equals(another.description)
        && dosage.equals(another.dosage)
        && unit.equals(another.unit)
        && timeToTake.equals(another.timeToTake);
  }

  /**
   * Computes a hash code from attributes: {@code id}, {@code description}, {@code dosage}, {@code unit}, {@code timeToTake}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + Objects.hashCode(id);
    h += (h << 5) + description.hashCode();
    h += (h << 5) + dosage.hashCode();
    h += (h << 5) + unit.hashCode();
    h += (h << 5) + timeToTake.hashCode();
    return h;
  }

  /**
   * Prints the immutable value {@code MedicationDto} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper("MedicationDto")
        .omitNullValues()
        .add("id", id)
        .add("description", description)
        .add("dosage", dosage)
        .add("unit", unit)
        .add("timeToTake", timeToTake)
        .toString();
  }

  /**
   * Creates an immutable copy of a {@link MedicationDto} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable MedicationDto instance
   */
  public static ImmutableMedicationDto copyOf(MedicationDto instance) {
    if (instance instanceof ImmutableMedicationDto) {
      return (ImmutableMedicationDto) instance;
    }
    return ImmutableMedicationDto.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link ImmutableMedicationDto ImmutableMedicationDto}.
   * <pre>
   * ImmutableMedicationDto.builder()
   *    .id(UUID | null) // nullable {@link MedicationDto#getId() id}
   *    .description(String) // required {@link MedicationDto#getDescription() description}
   *    .dosage(Float) // required {@link MedicationDto#getDosage() dosage}
   *    .unit(String) // required {@link MedicationDto#getUnit() unit}
   *    .timeToTake(java.time.LocalTime) // required {@link MedicationDto#getTimeToTake() timeToTake}
   *    .build();
   * </pre>
   * @return A new ImmutableMedicationDto builder
   */
  public static ImmutableMedicationDto.Builder builder() {
    return new ImmutableMedicationDto.Builder();
  }

  /**
   * Builds instances of type {@link ImmutableMedicationDto ImmutableMedicationDto}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @Generated(from = "MedicationDto", generator = "Immutables")
  @NotThreadSafe
  public static final class Builder {
    private static final long INIT_BIT_DESCRIPTION = 0x1L;
    private static final long INIT_BIT_DOSAGE = 0x2L;
    private static final long INIT_BIT_UNIT = 0x4L;
    private static final long INIT_BIT_TIME_TO_TAKE = 0x8L;
    private long initBits = 0xfL;

    private @Nullable UUID id;
    private @Nullable String description;
    private @Nullable Float dosage;
    private @Nullable String unit;
    private @Nullable LocalTime timeToTake;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code MedicationDto} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(MedicationDto instance) {
      Objects.requireNonNull(instance, "instance");
      @Nullable UUID idValue = instance.getId();
      if (idValue != null) {
        id(idValue);
      }
      description(instance.getDescription());
      dosage(instance.getDosage());
      unit(instance.getUnit());
      timeToTake(instance.getTimeToTake());
      return this;
    }

    /**
     * Initializes the value for the {@link MedicationDto#getId() id} attribute.
     * @param id The value for id (can be {@code null})
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("id")
    public final Builder id(@Nullable UUID id) {
      this.id = id;
      return this;
    }

    /**
     * Initializes the value for the {@link MedicationDto#getDescription() description} attribute.
     * @param description The value for description 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("description")
    public final Builder description(String description) {
      this.description = Objects.requireNonNull(description, "description");
      initBits &= ~INIT_BIT_DESCRIPTION;
      return this;
    }

    /**
     * Initializes the value for the {@link MedicationDto#getDosage() dosage} attribute.
     * @param dosage The value for dosage 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("dosage")
    public final Builder dosage(Float dosage) {
      this.dosage = Objects.requireNonNull(dosage, "dosage");
      initBits &= ~INIT_BIT_DOSAGE;
      return this;
    }

    /**
     * Initializes the value for the {@link MedicationDto#getUnit() unit} attribute.
     * @param unit The value for unit 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("unit")
    public final Builder unit(String unit) {
      this.unit = Objects.requireNonNull(unit, "unit");
      initBits &= ~INIT_BIT_UNIT;
      return this;
    }

    /**
     * Initializes the value for the {@link MedicationDto#getTimeToTake() timeToTake} attribute.
     * @param timeToTake The value for timeToTake 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("timeToTake")
    public final Builder timeToTake(LocalTime timeToTake) {
      this.timeToTake = Objects.requireNonNull(timeToTake, "timeToTake");
      initBits &= ~INIT_BIT_TIME_TO_TAKE;
      return this;
    }

    /**
     * Builds a new {@link ImmutableMedicationDto ImmutableMedicationDto}.
     * @return An immutable instance of MedicationDto
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableMedicationDto build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutableMedicationDto(id, description, dosage, unit, timeToTake);
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = new ArrayList<>();
      if ((initBits & INIT_BIT_DESCRIPTION) != 0) attributes.add("description");
      if ((initBits & INIT_BIT_DOSAGE) != 0) attributes.add("dosage");
      if ((initBits & INIT_BIT_UNIT) != 0) attributes.add("unit");
      if ((initBits & INIT_BIT_TIME_TO_TAKE) != 0) attributes.add("timeToTake");
      return "Cannot build MedicationDto, some of required attributes are not set " + attributes;
    }
  }
}
