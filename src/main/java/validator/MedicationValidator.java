package validator;

import exception.ValidationException;
import model.medication.Medication;

public class MedicationValidator {

    public static void validateMedication(Medication medication) {
        String msg = "";
        if (medication.getDescription().length() < 3) {
            msg += "Medication description can not be less than 3 characters! \n";
        }
        if (!msg.equals("")) {
            throw new ValidationException(msg);
        }
    }
}
