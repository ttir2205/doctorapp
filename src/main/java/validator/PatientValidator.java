package validator;

import exception.ValidationException;
import model.patient.Patient;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Objects;

public class PatientValidator {

    public static void validatePatient(Patient patient) {
        String msg = "";
        if (Objects.isNull(patient.getFirstName()) || patient.getFirstName().equals("")) {
            msg += "Patient's first name can not be null! \n";
        }
        if (Objects.isNull(patient.getLastName()) || patient.getLastName().equals("")) {
            msg += "Patient's last name can not be null! \n";
        }
        if (Period.between(patient.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), LocalDate.now()).getYears() < 18) {
            msg += "Patient's age has to be greater than 18! \n";
        }
        if (!msg.equals("")) {
            throw new ValidationException(msg);
        }
    }
}
