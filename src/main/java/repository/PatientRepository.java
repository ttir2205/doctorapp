package repository;

import model.patient.Patient;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PatientRepository extends BaseRepository<Patient> {

    Optional<Patient> findById(UUID id);

}
