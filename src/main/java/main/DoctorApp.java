package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@CrossOrigin
@ComponentScan(basePackages = {
        "config",
        "controller",
        "service",
        "model"
})
@EnableWebMvc
@EnableJpaRepositories(basePackages = {"repository"})
@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
@EnableScheduling
public class DoctorApp {
    public static void main(String[] args) {
        SpringApplication.run(DoctorApp.class, args);
    }
}