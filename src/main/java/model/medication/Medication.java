package model.medication;

import model.patient.Patient;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.*;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue
    @Column(name = "id", columnDefinition = "BINARY(16)", updatable = false)
    private UUID id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    private Date createDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private Date modifyDate;

    @Column
    private String description;

    @Column
    private Float dosage;

    @Column
    @Enumerated(EnumType.STRING)
    private Unit unit;

    @Basic
    @Column(name = "time_to_take")
    private LocalTime timeToTake;

    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    public UUID getId() {
        return id;
    }

    public Float getDosage() {
        return dosage;
    }

    public void setDosage(Float dosage) {
        this.dosage = dosage;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public LocalTime getTimeToTake() {
        return timeToTake;
    }

    public void setTimeToTake(LocalTime timeToTake) {
        this.timeToTake = timeToTake;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
