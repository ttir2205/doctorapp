package controller;

import dto.MedicationDto;
import dto.PatientDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.PatientService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @PostMapping
    public ResponseEntity<PatientDto> addPatient(@RequestBody PatientDto patientDto) {
        return ResponseEntity.ok(patientService.addPatient(patientDto));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PatientDto> updatePatient(@RequestBody PatientDto patientDto, @PathVariable("id") UUID id) {
        return ResponseEntity.ok(patientService.updatePatient(id, patientDto));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDto> getPatient(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(patientService.getPatient(id));
    }

    @GetMapping
    public ResponseEntity<List<PatientDto>> getAllPatients(@RequestParam(value = "sort", required = false) String sort, @RequestParam(value = "searchTerm", required = false) String searchName) {
        return ResponseEntity.ok(patientService.getAllPatients(sort, searchName));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deletePatient(@PathVariable("id") UUID id) {
        patientService.deletePatient(id);
        return ResponseEntity.ok(null);
    }

    @PostMapping(value = "/{patientId}/medication")
    public ResponseEntity<MedicationDto> addMedication(@PathVariable("patientId") UUID patientId, @RequestBody MedicationDto medicationDto) {
        return ResponseEntity.ok(patientService.addMedication(patientId, medicationDto));
    }

    @PutMapping(value = "/{patientId}/medication/{medicationId}")
    public ResponseEntity<MedicationDto> updateMedication(@PathVariable("patientId") UUID patientId, @PathVariable("medicationId") UUID medicationId, @RequestBody MedicationDto medicationDto) {
        return ResponseEntity.ok(patientService.updateMedication(patientId, medicationId, medicationDto));
    }

    @DeleteMapping(value = "/{patientId}/medication/{medicationId}")
    public ResponseEntity<Void> deleteMedication(@PathVariable("patientId") UUID patientId, @PathVariable("medicationId") UUID medicationId) {
        patientService.deleteMedication(patientId, medicationId);
        return ResponseEntity.ok(null);
    }

    @GetMapping(value = "{patientId}/medication")
    public ResponseEntity<List<MedicationDto>> getMedicationsForPatient(@PathVariable("patientId") UUID patientId) {
        return ResponseEntity.ok(patientService.getMedicationsForPatient(patientId));
    }

}
