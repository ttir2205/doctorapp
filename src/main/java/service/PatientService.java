package service;

import exception.EntityNotFoundException;
import dto.ImmutableMedicationDto;
import dto.ImmutablePatientDto;
import dto.MedicationDto;
import dto.PatientDto;
import model.medication.Medication;
import model.medication.Unit;
import model.patient.Gender;
import model.patient.Patient;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repository.PatientRepository;
import validator.MedicationValidator;
import validator.PatientValidator;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @PersistenceContext
    private EntityManager entityManager;

    private Logger log = LoggerFactory.getLogger(PatientService.class);

    private Patient mapPatientDtoToPatient(Patient patient, PatientDto patientDto) {
        patient.setFirstName(patientDto.getFirstName());
        patient.setLastName(patientDto.getLastName());
        patient.setGender(Gender.valueOf(patientDto.getGender()));
        patient.setBirthDate(patientDto.getBirthDate());
        PatientValidator.validatePatient(patient);
        return patient;
    }

    private PatientDto mapPatientToPatientDto(Patient patient) {
        return ImmutablePatientDto.builder()
                .id(patient.getId())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .birthDate(patient.getBirthDate())
                .gender(String.valueOf(patient.getGender()))
                .build();
    }

    private PatientDto mapPatientToPatientDtoWithMedications(Patient patient) {
        return ImmutablePatientDto.builder()
                .id(patient.getId())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .birthDate(patient.getBirthDate())
                .gender(String.valueOf(patient.getGender()))
                .medications(patient.getMedications().stream().map(medication -> mapMedicationToMedicationDto(medication)).collect(Collectors.toList()))
                .build();
    }

    private Medication mapMedicationDtoToMedication(Medication medication, MedicationDto medicationDto) {
        medication.setDescription(medicationDto.getDescription());
        medication.setUnit(Unit.valueOf(medicationDto.getUnit()));
        medication.setDosage(medicationDto.getDosage());
        medication.setTimeToTake(medicationDto.getTimeToTake());
        MedicationValidator.validateMedication(medication);
        return medication;
    }

    private MedicationDto mapMedicationToMedicationDto(Medication medication) {
        return ImmutableMedicationDto.builder()
                .id(medication.getId())
                .description(medication.getDescription())
                .dosage(medication.getDosage())
                .unit(String.valueOf(medication.getUnit()))
                .timeToTake(medication.getTimeToTake())
                .build();
    }

    public PatientDto addPatient(PatientDto patientDto) {
        log.info("Adding patient ...");
        Patient patient = patientRepository.save(mapPatientDtoToPatient(new Patient(), patientDto));
        return mapPatientToPatientDto(patient);
    }

    @Transactional
    public PatientDto updatePatient(UUID id, PatientDto patientDto) {
        log.info("Updating patient with id {} ...", id);
        Patient patient = patientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Patient with id " + id + " not found!"));
        mapPatientDtoToPatient(patient, patientDto);
        return mapPatientToPatientDto(patient);
    }

    @Transactional
    public PatientDto getPatient(UUID id) {
        log.info("Getting patient with id {} ...", id);
        Patient patient = patientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Patient with id " + id + " not found!"));
        return mapPatientToPatientDtoWithMedications(patient);
    }

    public List<PatientDto> getAllPatients(String sort, String searchName) {
        log.info("Getting patients ...");
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Patient> criteriaQuery = criteriaBuilder.createQuery(Patient.class);
        final Root<Patient> root = criteriaQuery.from(Patient.class);
        javax.persistence.criteria.Predicate disjunction = criteriaBuilder.disjunction();
        criteriaQuery.select(root).distinct(true);
        if (Objects.nonNull(searchName)) {
            disjunction.getExpressions().add(criteriaBuilder.like(root.get("firstName"), "%" + searchName + "%"));
            disjunction.getExpressions().add(criteriaBuilder.like(root.get("lastName"), "%" + searchName + "%"));
            criteriaQuery.where(disjunction);
        }

        List<Order> orderList = new ArrayList<>();
        if (Objects.nonNull(sort)) {
            orderList.add(criteriaBuilder.asc(root.get(sort)));
            criteriaQuery.orderBy(orderList);
        }
        return entityManager.createQuery(criteriaQuery).setFlushMode(FlushModeType.COMMIT).getResultList()
                .stream().map(patient -> mapPatientToPatientDto(patient)).collect(Collectors.toList());
    }

    public void deletePatient(UUID id) {
        log.info("Deleting patient with id {} ...", id);
        patientRepository.delete(id);
    }

    @Transactional
    public MedicationDto addMedication(UUID patientId, MedicationDto medicationDto) {
        log.info("Adding medication for patient with id {} ...", patientId);
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new EntityNotFoundException("Patient with id " + patientId + " not found!"));
        Medication medication = mapMedicationDtoToMedication(new Medication(), medicationDto);
        patient.addMedication(medication);
        medication.setPatient(patient);
        return mapMedicationToMedicationDto(medication);
    }

    @Transactional
    public MedicationDto updateMedication(UUID patientId, UUID medicationId, MedicationDto medicationDto) {
        log.info("Updating medication with id {} for patient with id {} ...", medicationId, patientId);
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new EntityNotFoundException("Patient with id " + patientId + " not found!"));
        Medication medicationToUpdate = patient.getMedications().stream()
                .filter(medication -> medication.getId().equals(medicationId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Medication with id" + medicationId + " not found!"));
        mapMedicationDtoToMedication(medicationToUpdate, medicationDto);
        return mapMedicationToMedicationDto(medicationToUpdate);
    }

    @Transactional
    public void deleteMedication(UUID patientId, UUID medicationId) {
        log.info("Deleting medication with id {} for patient with id {} ...", medicationId, patientId);
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new EntityNotFoundException("Patient with id " + patientId + " not found!"));
        Medication medicationToDelete = patient.getMedications().stream()
                .filter(medication -> medication.getId().equals(medicationId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Medication with id" + medicationId + " not found!"));
        patient.getMedications().remove(medicationToDelete);
    }

    @Transactional
    public List<MedicationDto> getMedicationsForPatient(UUID patientId) {
        log.info("Getting medications for patient with id {} ...", patientId);
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new EntityNotFoundException("Patient with id " + patientId + " not found!"));
        return patient.getMedications().stream().map(medication -> mapMedicationToMedicationDto(medication)).collect(Collectors.toList());
    }

    @Scheduled(cron = "0 0/5 * * * *")
    @Transactional
    public void announcePatients() {
        final String localTime = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
        log.info("Executing job ...");
        List<Patient> patients = patientRepository.findAll();
        patients.forEach(patient -> {
            patient.getMedications().forEach(medication -> {
                if (String.valueOf(medication.getTimeToTake()).equals(localTime)) {
                    log.info("{} {} has to take {} {} of {}", patient.getFirstName(), patient.getLastName(), medication.getDosage(), medication.getUnit(), medication.getDescription());
                }
            });
        });
    }
}
