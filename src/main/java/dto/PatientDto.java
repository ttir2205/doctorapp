package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javax.annotation.Nullable;
import org.immutables.value.Value;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Value.Immutable
@JsonDeserialize(builder = ImmutablePatientDto.Builder.class)
public interface PatientDto {

    String getFirstName();

    String getLastName();

    String getGender();

    Date getBirthDate();

    @Nullable
    UUID getId();

    @Nullable
    List<MedicationDto> getMedications();
}
