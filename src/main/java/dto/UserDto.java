package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javax.annotation.Nullable;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ImmutableUserDto.Builder.class)
public interface UserDto {

    String getUsername();

    @Nullable
    String getPassword();

}
