package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javax.annotation.Nullable;
import org.immutables.value.Value;

import java.time.LocalTime;
import java.util.UUID;

@Value.Immutable
@JsonDeserialize(builder = ImmutableMedicationDto.Builder.class)
public interface MedicationDto {

    @Nullable
    UUID getId();

    String getDescription();

    Float getDosage();

    String getUnit();

    LocalTime getTimeToTake();
}
